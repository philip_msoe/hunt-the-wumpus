//
// Created by tnugg on 5/21/2023.
//


#include <iostream>
#include "gameIO.h"
#include "player.h"


int main() {
    Player p = Player();
    p.init();

    GameIO gio(&p, reinterpret_cast<std::istream *>(&std::cout), reinterpret_cast<std::ostream *>(&std::cin));

    gio.print_map_unhidden();
    printf("Enter a command (-h for a list of commands): \n");
    int turn_count = 0;
    while (!p.win && !p.dead && turn_count < 150 && !gio.quit) {
        turn_count = turn_count + 1;
        gio.print_map();
        Player::print_proximity_messages(&p.map, p.cur_x, p.cur_y);

        while(!gio.input());
        printf("\n");
    }

    if (p.win) {
        printf("You won!\n");
    } else if (p.dead) {
        printf("You have died :(\n");
    } else {
        printf("You have run out of turns... how...?\n");
    }
    printf("End Map: \n");
    gio.print_map_unhidden();

    return 0;
}