//
// Created by tnugg on 5/21/2023.
//

#include <fstream>
#include <ostream>
#include <iostream>
#include "player.h"

class Command {
public:
    explicit Command(Player * p) {
        player = p;
    }

    virtual bool execute(std::ostream *out) = 0;

protected:
    Player * player;
};

class DirCommand: public Command {
public:
    DirCommand(Player * p, char d) : Command(p) {
        dir = d;
    }

protected:
    char dir;
};

class Move: public DirCommand {
public:
    Move(Player * p, char d) : DirCommand(p, d) {}

    bool execute(std::ostream *out) override {
        int new_x, new_y;
        switch (dir) {
            case 'n':
                new_x = player->cur_x;
                new_y = player->cur_y - 1;
                break;
            case 's':
                new_x = player->cur_x;
                new_y = player->cur_y + 1;
                break;
            case 'e':
                new_x = player->cur_x + 1;
                new_y = player->cur_y;
                break;
            case 'w':
                new_x = player->cur_x - 1;
                new_y = player->cur_y;
                break;
            default:
                return false;
        }

        bool move = player->move(new_x, new_y);
        if (player->dead) {
            Player::print_death_message(&player->map, new_x, new_y);
            return true;
        }

        if (move) {
            Player::print_proximity_messages(&player->map, new_x, new_y);
        } else {
            std::cout << "You ran face first into a rock...\n";
            player->map.discovered[new_x][new_y] = true;
        }

        return true;
    }
};

class Bow: public DirCommand {
public:
    Bow(Player * p, char d) : DirCommand(p, d) {}

    bool execute(std::ostream *out) override {
        if (player->arrow_count <= 0) {
            printf("Out of arrows!\n");
            return false;
        }

        player->decrement_arrows();
        int x,y;
        for (int i = 0; i < 3; i++) {
            switch (dir) {
                case 'n':
                    x = player->cur_x;
                    y = player->cur_y - (i+1);
                    break;
                case 's':
                    x = player->cur_x;
                    y = player->cur_y + (i+1);
                    break;
                case 'e':
                    x = player->cur_x + (i+1);
                    y = player->cur_y;
                    break;
                case 'w':
                    x = player->cur_x - (i+1);
                    y = player->cur_y;
                    break;
                default:
                    return false;
            }
            player->set_discovered(x,y);
            if (player->map.map[x][y] != EMPTY) {
                break;
            }
        }
        switch(player->map.map[x][y]) {
            case BAT:
                player->empty_cell(x,y);
                printf("A sudden cacophony of screeches, then the flapping stops.\n");
                break;
            case WUMPUS:
                player->win = true;
                printf("You have slain the mighty Wumpus with your bow!\n");
                break;
            case ROCK:
                printf("Your arrow plinks against a rock, breaking.\n");
                break;
            default:
                printf("Your arrow flies off into distance.\n");
                break;
        }

        return true;
    }
};

class Knife: public DirCommand {
public:
    Knife(Player * p, char d) : DirCommand(p, d) {}

    bool execute(std::ostream *out) override {
        if (player->knife_broken) {
            printf("You pull out your knife, to see its blade is snapped off. (it is unusable)\n");
            return false;
        }

        int x,y;
        switch (dir) {
            case 'n':
                x = player->cur_x;
                y = player->cur_y - 1;
                break;
            case 's':
                x = player->cur_x;
                y = player->cur_y + 1;
                break;
            case 'e':
                x = player->cur_x + 1;
                y = player->cur_y;
                break;
            case 'w':
                x = player->cur_x - 1;
                y = player->cur_y;
                break;
            default:
                return false;
        }
        player->map.discovered[x][y] = true;

        switch(player->map.map[x][y]) {
            case BAT:
                player->map.map[x][y] = EMPTY;
                printf("A sudden cacophony of screeches, then the flapping stops.\n");
                break;
            case WUMPUS:
                player->win = true;
                printf("You have slain the mighty Wumpus with your knife!\n");
                break;
            case ROCK:
                player->knife_broken = true;
                printf("You stab your knife into a rock and the blade snaps clean off.\n");
                break;
            default:
                printf("You stab empty air...\n");
                return false;
        }

        return true;
    }
};

class Quit: public Command {
public:
    explicit Quit(Player * p) : Command(p) {}

    bool execute(std::ostream *out) override {
        printf("Game Ended.\n");
        exit(0);
    }
};

class Inventory: public Command {
public:
    explicit Inventory(Player * p) : Command(p) {}

    bool execute(std::ostream *out) override {
        printf("Arrows Remaining: %d\n", player->arrow_count);
        return true;
    }
};
