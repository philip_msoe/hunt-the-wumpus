//
// Created by tnugg on 5/21/2023.
//

#include <fstream>

#ifndef HUNT_THE_WUMPUS_PLAYER_H
#define HUNT_THE_WUMPUS_PLAYER_H
#define MAP_H 6
#define MAP_W 6

#define ROCK '#'
#define EMPTY ' '
#define WUMPUS 'W'
#define BAT 'B'
#define PIT 'O'
#define PLAYER 'P'
#define ARROW 'A'

struct Map {
    char map[MAP_W][MAP_H];
    bool discovered[MAP_W][MAP_H];
};

class Player {
public:
    Map map = Map();
    int arrow_count = 5;
    int cur_x = -1, cur_y = -1, old_x = -1, old_y = -1;
    bool dead = false;
    bool win = false;
    bool knife_broken = false;

    Player() = default;

    bool move(int x, int y);
    void init();

    void decrement_arrows();

    void increment_arrows();

    void set_discovered(int x, int y);

    void empty_cell(int x, int y);

    static void print_proximity_messages(Map * m, int x, int y);

    static void print_death_message(Map * m, int x, int y);

    static bool is_cell_empty(Map * m, int x, int y);
private:
    void generate_map();

    void place();

    static char get_cell_content(Map * m, int x, int y);

    static void set_cell_content(Map * m, int x, int y, char c);

//    static Map init_map();

    static void destroy_map(Map * m);

    static bool is_cell_deadly(Map * m, int x, int y);

    static std::string get_death_message(char c);

    static std::string get_proximity_message(char c);

};


#endif //HUNT_THE_WUMPUS_PLAYER_H
