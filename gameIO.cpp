//
// Created by tnugg on 5/21/2023.
//
#include <cstdio>
#include <cstring>
#include <fstream>
#include "gameIO.h"
#include <commands.cpp>
#include <iostream>

#define MAP_H 6
#define MAP_W 6

GameIO::GameIO(Player *p, std::istream * i, std::ostream * o) {
    player = p;
    in = i;
    out = o;

    p->init();
}

void GameIO::command(char *cmd) {
    char str[MAXLEN];
    sprintf(str, "%s", cmd);

    std::string tst = str;

    if (tst.length() == 1) {
        switch (cmd[0]) {
            case 'n':
                execute('m', 'n');
                break;
            case 'e':
                execute('m', 'e');
                break;
            case 's':
                execute('m', 's');
                break;
            case 'w':
                execute('m', 'w');
                break;
        }
        return;
    }

    if (tst.find('-') == std::string::npos) {
        printf("Malformed command, improper format.\n");
        printf("Enter a command (-h for a list of commands): \n");
        return;
    }

    char* c = strtok(str, "-");

    if (c[0] == 'i' || c[0] == 'q' || c[0] == 'r' || c[0] == 'h') {
        execute(c[0], ' ');
        return;
    }

    char* d = strtok(NULL, "-");

    std::string s = c;
    if (s.empty()) {
        printf("No command entered.\n");
        printf("Enter a command (-h for a list of commands): \n");
        return;
    }

    if (c[0])

    execute(c[0], d[0]);
}

bool GameIO::execute(char cmd, char dir) {
    if (!validate(cmd, dir)) {
        return false;
    }

    bool result = false;

    if (cmd == 0) {

    }

    if (cmd == 'h') {
        std::cout   << "-h   \t| shows a list of commands\n"
                    << "-m -D\t| move you in a direction, n,s,e,w. Replace -D with a direction, like this: -m -n\n"
                    << "-k -D\t| knife in a direction, has range 1. Replace -D with a direction, like this: -k -n\n"
                    << "-b -D\t| Shoot your bow in a direction, has a range of 3, stops after it hits. Replace -D with a direction.\n"
                    << "-q   \t| quit, ending the game\n"
                    << "-r   \t| restart the game.\n"
                    << "-i   \t| check your inventory, tells you how many arrows you have left."
                    <<   "\n\n\n"
                    << "--- Entities ---\n"
                    << "Bats:\n"
                    << "\t Symbol: B\n"
                    << "\t Proximity Message: \"You hear a sound like a hundred flapping wings.\"\n"
                    <<"\t Killed Message:    \"A sudden cacophony of screeches, then the flapping stops.\"\t - Bats can be killed with your bow or your knife\n"
                    << "\t - If you move into it you will be transported to a random location on the map.\n\n"
                    << "Wumpus:\n"
                    << "\t Symbol: W\n"
                    << "\t Proximity Message: \"\"\n"
                    << "\t Killed Message:    \"You have slain the mighty Wumpus with your bow!\"\n"
                    << "\t Killed  By Message:\"You have unwittingly bumbled into the Wumpus itself! The stench alone killed you.\"\n"
                    << "\t - You win when you kill it, which can be done with knife or bow.\n"
                    << "\t - If you move into it you will die and lose.\t - Very VERY stinky.\n\n"
                    << "Arrows:\n"
                    << "\t Symbol: A\n"
                    << "\t Proximity Message: \"You smell a terrible stench.\"\n"
                    << "\t - When you move into a cell with an arrow in it, you will get another arrow to shoot with your bow.\n"
                    << "\t - You can see how many arrows you have with the -i command\n\n"
                    << "Pits:\n"
                    << "\t Symbol: O\n"
                    << "\t Proximity Message: \"You feel a cool breeze.\"\n"
                    << "\t Killed By Message:\"You tripped and fell down a bottomless pit. It takes a few weeks but eventually your rations run out and you starve.\"\n\n"
                    << "Rocks:\n"
                    << "\t Symbol: #\n"
                    << "\t - Cannot be moved through.\n"
                    << "\t - Will break your knife if you hit it, rendering it permanently unusable.\n"
                    << "\t - Your arrow will plink off and break when shot by your bow.\n"
                    << "\t - Hit by arrow message: \"Your arrow plinks against a rock, breaking.\"\n\n";
    } else if (cmd == 'm') {
        Move move(player, dir);
        result = move.execute(out);
    } else if (cmd == 'b') {
        Bow bow(player, dir);
        result = bow.execute(out);
    } else if (cmd == 'k') {
        Knife knife(player, dir);
        result = knife.execute(out);
    } else if (cmd == 'q') {
        Quit qt(player);
        result = qt.execute(out);
    } else if (cmd == 'i') {
        Inventory inventory(player);
        result = inventory.execute(out);
    } else if (cmd == 'r') {
        printf("Restarting the game.\n");
        player->init();
    }

    return result;
}

bool GameIO::validate(char cmd, char dir) {
    bool is_command = false;
    bool is_dir = false;

    for (char c : command_prefixes) {
        is_command = (c == cmd) || is_command;
    }

    for (char c : directions) {
        is_dir = (c == dir) || is_dir;
    }

    return is_command || is_dir;
}

void GameIO::print_map() {
    std::string lines[MAP_H];
    for (int y = 0; y < MAP_H; y++) {
        char line[MAP_W+1];
        for (int x = 0; x < MAP_W; x++) {
            line[x] = (player->map.discovered[x][y]) ? player->map.map[x][y] : 'X';
        }
        lines[y] = line;
    }

    std::string boarder;
    boarder.resize(MAP_W*3 + 2, '-');
    boarder[0] = '+';
    boarder[MAP_W*3+1] = '+';

    printf("%s\n", boarder.c_str());
    for (auto & line : lines) {
        printf("|");
        for (char c : line) {
            printf( " %c ", c);
        }
        printf("|\n");
    }
    printf("%s\n", boarder.c_str());

}

void GameIO::print_map_unhidden() {
    std::string lines[MAP_H];
    for (int y = 0; y < MAP_H; y++) {
        char line[MAP_W+1];
        for (int x = 0; x < MAP_W; x++) {
            line[x] = player->map.map[x][y];
        }
        lines[y] = line;
    }

    std::string boarder;
    boarder.resize(MAP_W*3 + 2, '-');
    boarder[0] = '+';
    boarder[MAP_W*3+1] = '+';

    printf("%s\n", boarder.c_str());
    for (auto & line : lines) {
        printf("|");
        for (char c : line) {
            printf( " %c ", c);
        }
        printf("|\n");
    }
    printf("%s\n", boarder.c_str());

}

bool GameIO::input() {
    char buf[500];
    try {
        fgets(buf, MAXLEN, stdin);
        std::string cmd = buf;

        if (cmd.empty()) {
            return false;
        }

        command(const_cast<char *>(cmd.c_str()));
    } catch (...) {
        printf("Invalid Command, try \"-h for a list of commands\".\n");
    }

    return true;
}





