//
// Created by tnugg on 5/21/2023.
//
#include<iostream>
#include<cstdlib>
#include "player.h"



void Player::generate_map() {
//    init_map();
    srand(time(nullptr));
    int bat_count = 1 + (rand() % 5);
    int pit_count = 1 + (rand() % 5);
    int arrow_count = 1 + (rand() % 5);

    for (int y = 0; y < MAP_H; y++) {
        for (int x = 0; x < MAP_W; x++) {
            map.map[x][y] = (rand() % 5 == 0) ? ROCK : EMPTY;
            map.discovered[x][y] = false;
        }
    }

    for (int i = 0; i < bat_count; i++) {
        map.map[rand() % MAP_W][rand() % MAP_H] = BAT;
    }

    for (int i = 0; i < pit_count; i++) {
        map.map[rand() % MAP_W][rand() % MAP_H] = PIT;
    }

    for (int i = 0; i < arrow_count; i++) {
        map.map[rand() % MAP_W][rand() % MAP_H] = ARROW;
    }

    place();

    do  {
        map.map[rand() % MAP_W][rand() % MAP_H] = WUMPUS;
    } while (map.map[rand() % MAP_W][rand() % MAP_H] == PLAYER);
}

bool Player::move(int x, int y) {

    if (is_cell_deadly(&this->map, x, y)) {
        dead = true;
        return false;
    } else if (map.map[x][y] == BAT) {
        printf("You step into a cloud of bats, they lift and carry you somewhere!");
        place();
        return true;
    } else if (map.map[x][y] == ARROW) {
        increment_arrows();
        set_cell_content(&map, x, y , EMPTY);
    } else if (!is_cell_empty(&this->map, x, y)) {
        return false;
    }

    old_x = cur_x, old_y = cur_y;
    cur_x = x, cur_y = y;

    map.map[old_x][old_y] = EMPTY;
    map.map[cur_x][cur_y] = PLAYER;

    map.discovered[x][y] = true;

    return true;
}

void Player::init() {
    generate_map();
}

void Player::destroy_map(Map *m)  {
    free(m->map);
    free(m);
}

void Player::place() {
    srand(time(nullptr));
    int x, y;
    do {
        x = rand() % MAP_W;
        y = rand() % MAP_H;
    } while (map.map[x][y] == WUMPUS);


    cur_x = x, old_x = x;
    cur_y = y, old_y = y;
    map.map[x][y] = EMPTY;

    move(x, y);
}

bool Player::is_cell_empty(Map *m, int x, int y) {
    return m->map[x][y] == EMPTY;
}

bool Player::is_cell_deadly(Map *m, int x, int y) {
    if (m->map[x][y] == PIT || m->map[x][y] == WUMPUS) {
        return true;
    }
    return false;
}

std::string Player::get_death_message(char c) {
    if (c == WUMPUS) {
        return "You have unwittingly bumbled into the Wumpus itself! The stench alone killed you.\n";
    }
    if (c == PIT) {
        return "You tripped and fell down a bottomless pit. It takes a few weeks but eventually your rations run out and you starve.\n";
    }
    return "";
}

void Player::print_proximity_messages(Map *m, int x, int y) {
    std::pair<int, int> cells[] = {
                std::pair<int, int>(x+1, y-1),
                std::pair<int, int>(x+1, y),
                std::pair<int, int>(x+1, y+1),
                std::pair<int, int>(x, y-1),
                std::pair<int, int>(x, y+1),
                std::pair<int, int>(x-1, y-1),
                std::pair<int, int>(x-1, y),
                std::pair<int, int>(x-1, y+1),
            };

    for (std::pair<int, int> p : cells) {
        std::cout << get_proximity_message(get_cell_content(m, p.first, p.second));
    }
}

std::string Player::get_proximity_message(char c) {
    switch(c) {
        case BAT:
            return "You hear a sound like a hundred flapping wings.\n";
        case WUMPUS:
            return "You smell a terrible stench.\n";
        case PIT:
            return "You feel a cool breeze.\n";
        default:
            return "";
    }
}

char Player::get_cell_content(Map *m, int x, int y) {
    return m->map[x][y];
}

void Player::print_death_message(Map *m, int x, int y) {
    std::cout << get_death_message(get_cell_content(m, x, y));
}

void Player::decrement_arrows() {
    arrow_count = arrow_count - 1;
}

void Player::set_discovered(int x, int y) {
    map.discovered[x][y] = true;
}

void Player::empty_cell(int x, int y) {
    map.map[x][y] = EMPTY;
}

void Player::increment_arrows() {
    arrow_count = arrow_count + 1;
}

void Player::set_cell_content(Map *m, int x, int y, char c) {
    m->map[x][y] = c;
}

