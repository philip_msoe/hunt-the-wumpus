//
// Created by tnugg on 5/21/2023.
//
#include "player.h"
#include <fstream>
#include <istream>
#include <ostream>

#ifndef HUNT_THE_WUMPUS_GAMEIO_H
#define HUNT_THE_WUMPUS_GAMEIO_H

#define MAXLEN 500

class GameIO {
public:
    bool quit = false;

    explicit GameIO(Player * p, std::istream * i, std::ostream * o);
    bool input();
    void command(char * cmd);
    void print_map();
    void print_map_unhidden();

private:
    Player * player;
    char command_prefixes[6] = {'m', 'b', 'k', 'q', 'i', 'r' };
    char directions[5] = {'n', 's', 'e', 'w', ' '};

    std::istream * in;
    std::ostream * out;

    bool validate(char cmd, char dir);
    bool execute(char cmd, char dir);
};


#endif //HUNT_THE_WUMPUS_GAMEIO_H
